CREATE TABLE [Accelerator].[Color]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ColorName] [nvarchar] (max) COLLATE Romanian_CI_AS NOT NULL,
[RGB] [varchar] (20) COLLATE Romanian_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Accelerator].[Color] ADD CONSTRAINT [PK_Color_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
