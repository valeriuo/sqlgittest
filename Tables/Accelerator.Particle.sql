CREATE TABLE [Accelerator].[Particle]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[X] [decimal] (10, 2) NOT NULL,
[Y] [decimal] (10, 2) NOT NULL,
[Value] [nvarchar] (max) COLLATE Romanian_CI_AS NOT NULL,
[ColorId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Accelerator].[Particle] ADD CONSTRAINT [PK_Point_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [Accelerator].[Particle] ADD CONSTRAINT [FK_ParticleColor] FOREIGN KEY ([ColorId]) REFERENCES [Accelerator].[Color] ([Id])
GO
